<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

//require_once __DIR__.'/config_custom/autoloadext.php';

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';
//AutoLoadExt::registerThirdPartyAPIs($loader);

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
